"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.car = void 0;
class car {
    constructor(color) {
        this.color = color;
    }
    toString() {
        return `${this.color} Car`;
    }
}
exports.car = car;
