"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Race = void 0;
class Race {
    constructor(name) {
        this.name = name;
        this.cars = [];
    }
    addCar(car) {
        this.cars.push(car);
    }
    toString() {
        return `Race name : ${this.name} \n${this.cars.map(car => car.toString())}`;
    }
}
exports.Race = Race;
