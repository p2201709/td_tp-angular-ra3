
export class car {
    private readonly color: string;

    constructor(color: string) {
        this.color = color;
    }

    public toString(): string{
        return `${this.color} Car`;
    }
    
}
