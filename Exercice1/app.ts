import { car } from "./car";
import { Race } from "./race";

var car1 = new car("Green");
var car2 = new car("Red");
var race = new Race("Lyon");
race.addCar(car1);
race.addCar(car2);
console.log(race.toString());
