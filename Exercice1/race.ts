import { car } from "./car";

export class Race {
    private name: string;
    private cars: car[];
    constructor(name: string) {
        this.name = name;
        this.cars = [];
    }

    addCar(car: car) {
        this.cars.push(car);
    }

    public toString():string {
        return `Race name : ${this.name} \n${this.cars.map(car => car.toString())}`
    }
}