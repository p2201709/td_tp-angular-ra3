import { Component } from '@angular/core';

@Component({
  selector: 'app-alerte',
  standalone: true,
  templateUrl: './alerte.component.html',
  styleUrls: ['./alerte.component.css']
})
export class AlerteComponent {
  public actualNature: string = 'info';
  private natureStatus: number = 0;

  onButtonClick(): void {
    this.natureStatus = (this.natureStatus + 1) % 3;
    this.actualNature = this.getNatureByNumber(this.natureStatus);
  }

  private getNatureByNumber(number: number): string {
    switch (number) {
      case 0:
        return 'info';
      case 1:
        return 'warning';
      case 2:
        return 'valide';
      default:
        return 'info';
    }
  }
}
