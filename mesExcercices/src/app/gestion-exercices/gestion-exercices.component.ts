import { Component } from '@angular/core';
import { ExerciceModel } from '../models/exercice-model';

@Component({
  selector: 'app-gestion-exercices',
  standalone: true,
  imports: [],
  templateUrl: './gestion-exercices.component.html',
  styleUrl: './gestion-exercices.component.css'
})
export class GestionExercicesComponent {
  public tabExercices: ExerciceModel[];
  constructor() {
    this.tabExercices = [];
    this.tabExercices.push(
      {
        id: 2,
        nom: 'démarrage du joli gestionnaire de devoirs',
        dateDeRendu: new Date(2024, 11, 3),
        rendu: false
      },
      {
        id: 3,
        nom: 'ajout d\'un routeur',
        dateDeRendu: new Date(2024, 12, 22),
        rendu: false
      }
    )
  }

  toString(): string {
    return this.tabExercices.map(exercice => {
      const rendu = exercice.rendu ? 'rendu' : 'non rendu';
      return `Exercice ${exercice.id} : ${exercice.nom} | ${exercice.dateDeRendu.toDateString()} | ${rendu}`;
    }).join('\n'); // Join the mapped strings into a single string
  }

}
