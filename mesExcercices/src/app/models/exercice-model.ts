export interface ExerciceModel {
    id: number;
    nom: String;
    dateDeRendu: Date;
    rendu: boolean;
}
